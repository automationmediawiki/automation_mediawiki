`README FOR THE AUTOMATION PROCESS:`

Mediawiki URL Link to check : https://mediawiki.ml/mw-config/index.php

1.First we need to create 2 YAML files for
   * CI/CD Pipeline
   * Mediawiki Configuration

### `We need to create this two files in GITLAB`

2. We need to create an ubuntu server 

   `IP` : 13.235.69.130
   `Username` : ubuntu

`Pem` file has been added along with this File

3. We need to configure inbound in AWS console for Nodeport access.

# Steps needed to be undertaken in the server

1. We need to Install Docker (Optional) (It would be helpful for the kubernetes to fetch the file from local docker Image).
2. We need to Install Kubernetes ( kubelet, kubeadm and kubectl ).
3. We need to Install Gitlab Runner to have a connection between Gitlab Repository and Server.
4. To access gitlab repository from server, it is necessary to register gitlab runner using Key, Tag, and URL.
5. Once the above process has been completed, we need to cross-check whether everything is working fine.
6. Now, We need to navigate to CI/CD -> Pipeline -> Click Run Pipeline to execute the auto deployment.
